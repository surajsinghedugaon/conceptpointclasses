import 'package:conceptpointclasses/bottom%20navigation/bottom_navigation.dart';
import 'package:conceptpointclasses/screen%20pages/users_class.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UsersGoal extends StatefulWidget {
  const UsersGoal({super.key});
  @override
  State<UsersGoal> createState() => _UsersGoalState();
}

class _UsersGoalState extends State<UsersGoal> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(

        child:Stack(
          children: <Widget>[
            Positioned(
              top: 43,
              height:170,
              child: Container(
                width: 425,
                height: 120,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.deepPurple[100],
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20, top: 10),
                              child: Card(
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  child: IconButton(onPressed: () {
                                    Navigator.pop(context);
                                  }, icon: Icon(Icons.arrow_back_ios_new,size: 15,)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 15, left: 25),
                        child: Row(
                          children: [
                            Text("Select", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                            Text(" the", style: TextStyle(fontSize: 20),),
                            Text(" goal", style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                            Text(" you", style: TextStyle(fontSize: 20),),
                            Text(" want...", style: TextStyle(fontSize: 20),),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15, left: 25),
                        child: Row(
                          children: [
                            Text("Exam can be changed at anytime later", style: TextStyle(fontSize: 12),),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),

            Positioned(
              top: 180,
              width: 425,
              height: 800,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(8),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => BottomNavigationPw(),));
                    },
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person, ),
                                title: Text("NEET", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("Class 11 | Class 12 | Class 12+"),
                              ),
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person),
                                title: Text("IIT JEE", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("Class 11 | Class 12 | Class 12+"),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person),
                                title: Text("PW OnlyIAS", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("UPSC | State PSC"),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person),
                                title: Text("School Preparation", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("Science | Commerce | Humanities A Class 6-10"),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person),
                                title: Text("Govt Exams", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("Banking | SSC | Defence | Teaching | Railway | JAIIB & CAIIB"),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person),
                                title: Text("UG & PG Entrance Exam", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("MBA | IPMAT | IIT Jam & CSIR NET | Law | CUET"),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person),
                                title: Text("ESE GATE & Engineering", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("ESE | GATE | AE/JE"),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person),
                                title: Text("Finance Course", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("CA"),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8, right: 8,bottom: 2),
                          child: Container(
                            height: 80,
                            child: Card(
                              child: ListTile(
                                leading: Icon(Icons.person),
                                title: Text("PW Skills", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                                subtitle: Text("Coding"),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
            )
          ],
        ),
      ),
      // body: Stack(
      //       children: [
      //         Container(
      //           color: Colors.white,
      //           child: Padding(
      //             padding: const EdgeInsets.only(bottom: 650),
      //             child: Container(
      //               decoration: BoxDecoration(
      //                 color: Colors.deepPurple[100],
      //               ),
      //               child: Column(
      //                 children: [
      //                   Padding(
      //                     padding: const EdgeInsets.only(top: 50),
      //                     child: Row(
      //                       mainAxisAlignment: MainAxisAlignment.start,
      //                       children: [
      //                         Padding(
      //                           padding: const EdgeInsets.all(8.0),
      //                           child: Card(
      //                             child: Container(
      //                               height: 40,
      //                               width: 40,
      //                               child: IconButton(onPressed: () {
      //                                 Navigator.pop(context);
      //                               }, icon: Icon(Icons.arrow_back_ios_new,size: 20,)),
      //                             ),
      //                           ),
      //                         ),
      //
      //
      //                         // InkWell(child: Card(
      //                         //   child: Container(),
      //                         // ),
      //                         // onTap: () {
      //                         //
      //                         // },
      //                         // )
      //                       ],
      //                     ),
      //                   ),
      //
      //                   Padding(
      //                     padding: const EdgeInsets.only(top: 25, left: 25),
      //                     child: Row(
      //                       children: [
      //                         Text("Select", style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),),
      //                         Text(" the", style: TextStyle(fontSize: 25),),
      //                         Text(" goal", style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
      //                         Text(" you", style: TextStyle(fontSize: 25),),
      //                         Text(" want...", style: TextStyle(fontSize: 25),),
      //                       ],
      //                     ),
      //                   ),
      //                   Padding(
      //                     padding: const EdgeInsets.only(top: 30, left: 25),
      //                     child: Row(
      //                       children: [
      //                         Text("Exam can be changed at anytime later", style: TextStyle(fontSize: 15),),
      //                       ],
      //                     ),
      //                   )
      //                 ],
      //               ),
      //             ),
      //           ),
      //         ),
      //       ],
      //     )

    );
  }
}

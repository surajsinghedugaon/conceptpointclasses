import 'dart:async';

import 'package:conceptpointclasses/bottom%20navigation/bottom_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../phone auth/send_otp.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        const Duration(seconds: 6),
        () {
          _isUserLogin().then((value){
            if(value){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => BottomNavigationPw(),));
            }
            else{
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PhoneNumber(),));
            }
          });
        }
    );
  }

  Future<bool> _isUserLogin() async {
    var loginStatus = FirebaseAuth.instance.currentUser?.uid;
    if (loginStatus != null) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Image(image: NetworkImage("https://janral.com/wp-content/uploads/2023/04/Physicswallah-logo.webp"))
              Image.asset(
                'assets/images/cpclogo.png',
                scale: 3,
              )
            ],
          ),
        ],
      )),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../user modle/user_modle.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({super.key});

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {


  TextEditingController updateNameController = TextEditingController();
  TextEditingController updateEmailController = TextEditingController();


  //get user data
  UserModel? showUserData;

  @override
  void initState() {
    super.initState();
    retriveData().then((value) {
      setState(() {
        showUserData = value;
        updateNameController.text = showUserData?.name ?? "";
        updateEmailController.text = showUserData?.email ?? "";
      });

      //get for show number current user
      user = _auth.currentUser;
    });

  }

  Future<UserModel?> retriveData() async {
    var auth= FirebaseAuth.instance.currentUser?.uid;
    var user = await FirebaseFirestore.instance.collection("cpcusers").doc(auth).get();
    var userModel = UserModel.fromJson(user.data()!);
    return userModel;
  }

  FirebaseAuth _auth = FirebaseAuth.instance;
  User? user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[50],
      body: ListView(
        children: [
          Container(
            height: 230,
            color: Colors.deepPurple,

            child: ListView(
              children: [
                Row(
                  children: [
                    IconButton(onPressed: () {
                      Navigator.pop(context);
                    }, icon: Icon(Icons.arrow_back, color: Colors.white,size: 25,)),
                    Center(child: Text('My Profile', style: TextStyle(color: Colors.white, fontSize: 18),))
                  ],
                ),

                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Center(
                    child: SizedBox(
                      height: 100,
                      width: 100,
                      child : Stack(
                        children: [
                          // imageFile != null ?
                          // CircleAvatar(
                          //     radius: 50,
                          //     backgroundImage: FileImage(File(imageFile!.path))
                          // ):

                          CircleAvatar(
                            radius: 50,
                            child: Icon(Icons.person, size: 50,),
                            // backgroundImage: widget.imageUrl != "" ? NetworkImage(widget.imageUrl) : NetworkImage('https://media.licdn.com/dms/image/D4D03AQFo9m--m9fwmQ/profile-displayphoto-shrink_800_800/0/1680501705686?e=1689811200&v=beta&t=gDH9Za-Tz9IqY0NBE6znPshdtvx-cCMo3CrpJ6Epfvc')
                            // widget.imagePath != null ? FileImage(File(imageFile!.path)) : null,
                          ),
                          // const CircleAvatar(
                          //     radius: 100,
                          //     backgroundColor: Colors.white,
                          //     backgroundImage: NetworkImage('https://media.licdn.com/dms/image/D4D03AQFo9m--m9fwmQ/profile-displayphoto-shrink_800_800/0/1680501705686?e=1689811200&v=beta&t=gDH9Za-Tz9IqY0NBE6znPshdtvx-cCMo3CrpJ6Epfvc'),
                          //   ):
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: Container(
                                width: 35,
                                height: 35,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.white),
                                    borderRadius: BorderRadius.circular(100), color: Colors.green),
                                child: IconButton( icon: const Icon(Icons.edit, color: Colors.white,size: 20,),
                                  onPressed: () {
                                    showModalBottomSheet(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return Wrap(
                                          children: [
                                            ListTile(
                                              leading: const Icon(
                                                Icons.camera_alt,
                                                color: Colors.black,
                                              ),
                                              title: const Text('Camera'),
                                              onTap: () {
                                                // pickImage(ImageSource.camera);
                                                Navigator.pop(context);
                                              },
                                            ),
                                            ListTile(
                                              leading: const Icon(
                                                Icons.image,
                                                color: Colors.black,
                                              ),
                                              title: const Text('Gallery'),
                                              onTap: () {
                                                // pickImage(ImageSource.gallery);
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },)
                            ),
                          )
                        ],
                      ),

                    ),
                  ),
                ),

                Center(child: Text("${showUserData?.name}",style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),))
              ],

            ),


          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Your Details", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
                  IconButton(onPressed: () {

                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            scrollable: true,
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Edit Details"),
                                IconButton(onPressed: () {
                                  Navigator.pop(context);
                                }, icon: Icon(Icons.close))
                              ],
                            ),
                            content: Padding(
                              padding: const EdgeInsets.all(0),
                              child: Form(
                                child: Column(
                                  children: <Widget>[
                                    TextFormField(
                                      controller: updateNameController,
                                      keyboardType: TextInputType.name,
                                      decoration: InputDecoration(
                                          // hintText: "${showUserData?.name}",
                                          icon: Icon(Icons.person),
                                      ),
                                    ),

                                    SizedBox(
                                      height: 20,
                                    ),
                                    
                                    TextFormField(
                                        controller: updateEmailController,
                                      keyboardType: TextInputType.emailAddress,
                                      decoration: InputDecoration(
                                        // hintText: '${showUserData?.email}',
                                        icon: Icon(Icons.mail),
                                      ),
                                    ),

                                    SizedBox(
                                      height: 20,
                                    ),

                                    TextFormField(
                                      enabled: false,
                                      maxLength: 10,
                                      controller: TextEditingController(text: FirebaseAuth.instance.currentUser?.phoneNumber?.replaceAll("+91", "") ?? 'Not available',),
                                      decoration: InputDecoration(
                                        hintText: "Phone no.",
                                        icon: Icon(Icons.phone ),
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                            ),
                            actions: [
                              ElevatedButton(
                                  child: Text("Update & Save"),
                                  onPressed: () {
                                    userUpdateProfile();
                                  })
                            ],
                          );
                        });

                  }, icon: Icon(Icons.edit_square,color: Colors.deepPurple,))
                ],
              ),
            ),
          ),


          Container(
            height: 190,
            child: Card(
              child: ListView(
                children: [
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text('${showUserData?.name}'),
                  ),
                  Divider(
                    height: 5,
                    thickness: 1,
                  ),
                  ListTile(
                    leading: Icon(Icons.mail),
                    title: Text('${showUserData?.email}'),
                  ),
                  Divider(
                    height: 5,
                    thickness: 1,
                  ),
                  ListTile(
                    leading: Icon(Icons.call),
                    title: Text(FirebaseAuth.instance.currentUser?.phoneNumber?.replaceAll("+91", "") ?? 'Not available',),
                  ),
                ],
              ),

            ),
          ),

          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Text("Other Details", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            ),
          ),

          Container(
            height: 190,
            child: Card(
              child: ListView(
                children: [
                  ListTile(
                    leading: Icon(Icons.batch_prediction),
                    title: Text('12th - NEET'),
                  ),
                  Divider(
                    height: 5,
                    thickness: 1,
                  ),
                  ListTile(
                    leading: Icon(Icons.location_city),
                    title: Text('Saran, Bihar'),
                  ),
                  Divider(
                    height: 5,
                    thickness: 1,
                  ),
                  ListTile(
                    leading: Icon(Icons.location_pin),
                    title: Text('Bihar'),
                  ),
                ],
              ),
            ),
          ),


        ],
      ),
    );
  }

  userUpdateProfile() {
    var auth = FirebaseAuth.instance.currentUser?.uid;
    FirebaseFirestore.instance.collection("cpcusers").doc(auth).update({
      "name": updateNameController.text,
      "email": updateEmailController.text,
    }).then((value) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => UserProfile(),
          ));
    });
  }

}

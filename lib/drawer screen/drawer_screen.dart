import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../phone auth/send_otp.dart';
import '../profile screen/users_profile_update.dart';
import '../user modle/user_modle.dart';

class AppDrawer extends StatefulWidget {
  const AppDrawer({super.key});

  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {

  UserModel?showUserName;
  @override
  void initState() {
    super.initState();
    retriveData().then((value) {
      setState(() {
        showUserName = value;
      });
    });

  }

  Future<UserModel?> retriveData() async {
    var auth= FirebaseAuth.instance.currentUser?.uid;
    var user = await FirebaseFirestore.instance.collection("cpcusers").doc(auth).get();
    var userModel = UserModel.fromJson(user.data()!);
    return userModel;
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Container(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.deepPurple,
              ),

              child: ListView(
                children: [
                  CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 50,
                    child: Image.asset('assets/images/verifyotp.png'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Hii ${showUserName?.name}", style: TextStyle(color: Colors.white),),

                        IconButton(onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => UserProfile(),));
                        }, icon: Icon(Icons.edit_square, color: Colors.white,)),
                      ],
                    ),
                  ),
                ],
              ),
            ),


            ListTile(
              leading: Icon(Icons.book),
              title: const Text(' My Course '),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            Divider(),

            ListTile(
              leading: Icon(Icons.workspace_premium),
              title: const Text(' Go Premium '),
              onTap: () {
                Navigator.pop(context);
              },
            ),

            Divider(),

            ListTile(
              leading: Icon(Icons.video_label),
              title: const Text(' Saved Videos '),
              onTap: () {
                Navigator.pop(context);
              },
            ),

            Divider(),

            ListTile(
              leading: Icon(Icons.logout, color: Colors.red,),
              title: const Text(' Log Out '),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) =>  AlertDialog(
                    title: Row(
                      children: const [
                        Icon(Icons.logout),
                        Padding(
                          padding: EdgeInsets.all(0),
                          child: Text("Logout"),
                        ),
                      ],
                    ),

                    // icon: Icon(CupertinoIcons.delete),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text(
                          'No', style: TextStyle(color: Colors.red),),
                      ),
                      TextButton(onPressed: () {

                        FirebaseAuth.instance.signOut().then((value) {
                          Navigator.pushReplacement(context, MaterialPageRoute(
                            builder: (context) => const PhoneNumber(),));
                        });

                        Fluttertoast.showToast(msg: 'Logout Successful',
                            textColor: Colors.black,
                            toastLength: Toast.LENGTH_LONG,
                            backgroundColor: Colors.white);
                      },
                        child: const Text(
                          'Yes', style: TextStyle(color: Colors.red),),)
                    ],
                  ),);
              },
            ),

          ],
        ),
      ),
    ));
  }
}

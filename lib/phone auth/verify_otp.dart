// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:lottie/lottie.dart';
// import 'package:physics_wallah/phone%20auth/send_otp.dart';
// import 'package:pinput/pinput.dart';
//
// import '../bottom navigation/bottom_navigation.dart';
//
// class VerifyOtp extends StatefulWidget {
//   const VerifyOtp({Key? key}) : super(key: key);
//
//   @override
//   State<VerifyOtp> createState() => _VerifyOtpState();
// }
//
// class _VerifyOtpState extends State<VerifyOtp> {
//   TextEditingController pinController = TextEditingController();
//
//   FirebaseAuth auth = FirebaseAuth.instance;
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: SafeArea(
//         top: true,
//         child: Scaffold(
//           body: ListView(
//             children: [
//               Column(
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Padding(
//                     padding: const EdgeInsets.only(top: 50),
//                     child: Lottie.network("https://assets1.lottiefiles.com/packages/lf20_55kkjtkv.json"),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 10),
//                     child: Pinput(
//                       keyboardType: TextInputType.number,
//                       controller: pinController,
//                       length: 6,
//                       showCursor: true,
//                       pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
//                       textInputAction: TextInputAction.next,
//                     ),
//                   ),
//                   Padding(
//                       padding: const EdgeInsets.only(top: 30),
//                       child: SizedBox(
//                         height: 50,
//                         child: ElevatedButton(
//                             onPressed: () async {
//                               try {
//                                 PhoneAuthCredential credential = PhoneAuthProvider.credential(
//                                     verificationId: SendOtpPw.verify,
//                                     smsCode: pinController.text);
//                                 // Sign the user in (or link) with the credential
//                                 await auth.signInWithCredential(credential);
//                                 Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>  BottomNavigationPw()));
//                                 Fluttertoast.showToast(msg: "Login successful");
//                               } catch (e) {
//                                 Fluttertoast.showToast(msg: "Enter Valid OTP");
//                               }
//                             },
//                             style: ElevatedButton.styleFrom(
//                                 backgroundColor: Colors.green, side: BorderSide.none, shape: StadiumBorder()),
//                             child: const Text("Please Enter Otp")),
//                       ))
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

import 'dart:async';
import 'package:conceptpointclasses/phone%20auth/send_otp.dart';
import 'package:conceptpointclasses/user%20exists%20or_not/user_exist.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pinput/pinput.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../bottom navigation/bottom_navigation.dart';
import '../new user registration/new_users_registration.dart';
import '../screen pages/users_goal.dart';

class OtpPage extends StatefulWidget {

final phoneNumber;

   OtpPage({Key? key, required  this.phoneNumber }) : super(key: key);

  @override
  State<OtpPage> createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {

  //there show loding process after click on verify button
  bool isLoading = false;

  TextEditingController pinController = TextEditingController();
  FirebaseAuth auth = FirebaseAuth.instance;

  //there implement resend otp countdown
   int secondsRemaining = 60;
   bool enableResend = false;
   late Timer timer;


  //show user number code start
  int? number;

  @override
  void initState() {
    super.initState();
    //getData function there
    getData();

    timer = Timer.periodic(Duration(seconds: 1), (_) {
      if(secondsRemaining != 0){
        setState(() {
          secondsRemaining--;
        });
      }
      else{
        setState(() {
          enableResend = true;
        });
      }
    });
  }

  Future getData() async {
    var sharepref = await SharedPreferences.getInstance();
    setState(() {
      number = sharepref.getInt('number')!;
    });
  }

  //Here store user uId
  storeUid( String uid) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('uid', uid);
  }

  //show user number code start


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 150),
                child: Image.asset(
                  'assets/images/verifyotp.png',
                  scale: 2,
                ),
              ),
              Center(
                  child: Text(
                    "OTP Verification",
                    style: TextStyle(
                        color: Colors.deepPurple,
                        fontWeight: FontWeight.bold,
                        fontSize: 25),
                  )),
              SizedBox(
                height: 2,
              ),


              Wrap(
                alignment: WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  Text("Enter the Code sent on (+91) ",style: TextStyle(fontSize: 16),),
                  Text("$number", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                ],
              ),
              SizedBox(
                height: 1,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Pinput(
                  keyboardType: TextInputType.number,
                  controller: pinController,
                  length: 6,
                  showCursor: true,
                  pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                  textInputAction: TextInputAction.next,
                ),
              ),

              SizedBox(
                height: 15,
              ),

              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("after $secondsRemaining seconds"),
                  TextButton(
                      onPressed:() async {
                        try {
                          await FirebaseAuth.instance.verifyPhoneNumber(
                            phoneNumber: '+91${widget.phoneNumber}',
                            verificationCompleted: (PhoneAuthCredential credential) {},
                            verificationFailed: (FirebaseAuthException e) {},
                            codeSent: (String verificationId, int? resendToken) {
                              PhoneNumber.verify=verificationId;
                              Fluttertoast.showToast(msg: "Sent OTP");
                            },
                            codeAutoRetrievalTimeout: (String verificationId) {},
                          );
                          resendCode();

                        }
                        catch(e){
                          
                        }
                      },
                      child: Text("Resend OTP")),
                ],
              ),



              Padding(
                padding: const EdgeInsets.only(top: 40),
                child: Column(
                  children: [
                    Text("We are trying to retrive your OTP",style: TextStyle(fontSize: 16),),
                    Text("Please Wait... ",style: TextStyle(fontSize: 16),),
                  ],
                ),
              ),


              Padding(
                  padding: const EdgeInsets.only(top: 80),
                  child: SizedBox(
                    height: 50,
                    width: 200,
                    child: ElevatedButton(
                        onPressed: () async {
                          try {
                            PhoneAuthCredential credential = PhoneAuthProvider.credential(
                                verificationId: PhoneNumber.verify,
                                smsCode: pinController.text);
                            // Sign the user in (or link) with the credential
                            await auth.signInWithCredential(credential);
                            var uId = FirebaseAuth.instance.currentUser?.uid;
                            bool userExists = await checkUserExists(uId!);
                            storeUid(uId.toString());
                            if(userExists){
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => BottomNavigationPw(),));
                            }
                            else{
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => NewUsersRegistration(),));
                            }


                            Fluttertoast.showToast(msg: "Login successful");
                          } catch (e) {
                            Fluttertoast.showToast(msg: "Enter Valid OTP");
                          }
                        },

                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.deepPurple,
                            side: BorderSide.none,
                            shape: StadiumBorder()),
                        child: const Text("Verify Otp")),
                  ))
            ],
          )
        ],
      ),
    );
  }

  void resendCode() {
    //again resend count time code here
    setState((){
      secondsRemaining = 60;
      enableResend = true;
    });
  }

  @override
  dispose(){
    timer.cancel();
    super.dispose();
  }
}

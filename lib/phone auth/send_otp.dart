// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:lottie/lottie.dart';
// import 'package:physics_wallah/phone%20auth/verify_otp.dart';
//
// class SendOtpPw extends StatefulWidget {
//   const SendOtpPw({Key? key}) : super(key: key);
//
//   static String verify = "";
//
//   @override
//   State<SendOtpPw> createState() => _SendOtpPwState();
// }
//
// class _SendOtpPwState extends State<SendOtpPw> {
//
//   TextEditingController phoneController = TextEditingController();
//
//   // TextEditingController nameController=TextEditingController();
//
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: SafeArea(
//         top: true,
//         child: Scaffold(
//           body:
//           ListView(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Wrap(
//                   crossAxisAlignment: WrapCrossAlignment.start,
//                   alignment: WrapAlignment.spaceBetween,
//                   children: [
//                     // CircleAvatar(
//                     //   backgroundColor: Colors.white,
//                     //   radius: 50,
//                     //   child:
//                     Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         // Image(image: NetworkImage("https://janral.com/wp-content/uploads/2023/04/Physicswallah-logo.webp"))
//
//                         Image.asset('assets/images/cpclogo.png', scale: 4,)
//                       ],
//                     ),
//                     // ),
//                     Lottie.network(
//                         "https://assets6.lottiefiles.com/private_files/lf30_TBKozE.json",
//                         height: 250, width: 250),
//
//                   ],
//                 ),
//               ),
//
//               Center(child: Text("Welcome", style: TextStyle(
//                   color: Colors.deepPurpleAccent,
//                   fontWeight: FontWeight.bold,
//                   fontSize: 25),)),
//               SizedBox(
//                 height: 5,
//               ),
//
//               Center(child: Text("to Concept Point Classes", style: TextStyle(
//                   fontWeight: FontWeight.bold,
//                   fontSize: 25,
//                   letterSpacing: 0.1),)),
//               SizedBox(
//                 height: 10,
//               ),
//
//               Center(child: Text(
//                 "Best Teachers | Affordable Pricing | Live Batches | DPP | Notes",
//                 style: TextStyle(fontSize: 10,),)),
//               SizedBox(
//                 height: 25,
//               ),
//               Padding(
//                 padding: const EdgeInsets.all(15),
//                 child: TextField(
//                   controller: phoneController,
//                   keyboardType: TextInputType.phone,
//                   maxLength: 10,
//                   decoration: InputDecoration(
//                       border: OutlineInputBorder(
//                         borderRadius: BorderRadius.circular(5),
//                       ),
//                       prefixIcon: Icon(Icons.phone),
//                       labelText: "Enter your Phone Number"
//                   ),
//                 ),
//               ),
//
//               // Padding(
//               //   padding: const EdgeInsets.only(top: 5, left: 15, right: 15),
//               //   child: TextField(
//               //     controller: nameController,
//               //     keyboardType: TextInputType.name,
//               //     decoration: InputDecoration(
//               //         border: OutlineInputBorder(
//               //           borderRadius: BorderRadius.circular(5),
//               //         ),
//               //         prefixIcon: Icon(Icons.person),
//               //         labelText: "Enter your name"
//               //     ),
//               //   ),
//               // ),
//
//               Center(child: Padding(
//                 padding: const EdgeInsets.only(top: 100),
//                 child: Text("Do You Have a Referral Code?",
//                   style: TextStyle(fontSize: 10),),
//               )),
//
//               Padding(
//                 padding: const EdgeInsets.only(left: 45, right: 45, top: 5),
//                 child: SizedBox(
//                   height: 55,
//                   child: TextField(
//                     textAlign: TextAlign.center,
//                     decoration: InputDecoration(
//                         hintText: "Enter Here (Optional)",
//                         border: OutlineInputBorder(
//                             borderSide: BorderSide(style: BorderStyle.solid),
//                             borderRadius: BorderRadius.circular(10))
//                     ),
//                   ),
//                 ),
//               ),
//
//               Padding(
//                 padding: const EdgeInsets.all(25),
//                 child: SizedBox(
//                   height: 50,
//                   width: 50,
//                   child: ElevatedButton(
//                       style: ElevatedButton.styleFrom(
//                           backgroundColor: Colors.deepPurple
//                       ),
//                       onPressed: () async {
//                         try {
//                           await FirebaseAuth.instance.verifyPhoneNumber(
//                             phoneNumber: '+91${phoneController.text}',
//                               verificationCompleted: (PhoneAuthCredential credential){},
//                               verificationFailed: (FirebaseAuthException e){},
//                               codeSent: (String verificationId, int? resendToken){
//                                 SendOtpPw.verify = verificationId;
//                             Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const VerifyOtp(),));
//                             Fluttertoast.showToast(msg: "Sent Otp");
//                               },
//                               codeAutoRetrievalTimeout: (String verificationId){}
//                           );
//                         }
//                         catch (e) {
//                           Fluttertoast.showToast(msg: "Otp Failed");
//                         }
//                       }, child: Text("Continue")),
//                 ),
//               ),
//               Wrap(
//                 alignment: WrapAlignment.center,
//                 children: [
//                   Text("By Continuing you agree to our  ",
//                     style: TextStyle(fontSize: 11),),
//                   Text("Terms of Use & Privacy Policy", style: TextStyle(
//                       color: Colors.deepPurple,
//                       fontSize: 11,
//                       decoration: TextDecoration.underline),)
//                 ],
//               )
//
//             ],
//           ),
//         ),
//       ),
//
//     );
//   }
// }
//
import 'package:firebase_core/firebase_core.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:conceptpointclasses/phone%20auth/verify_otp.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';


class PhoneNumber extends StatefulWidget {
  const PhoneNumber({Key? key}) : super(key: key);

  static String verify = "";

  @override
  State<PhoneNumber> createState() => _PhoneNumberState();
}

class _PhoneNumberState extends State<PhoneNumber> {

  TextEditingController phoneController=TextEditingController();

  //there store number in sharepreference
  storeNumber() async {
    final prefs = await SharedPreferences.getInstance();
    int numberValue = int.tryParse(phoneController.text) ?? 0;
    prefs.setInt('number', numberValue);
  }
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ListView(
          children: [
        Padding(
                padding: const EdgeInsets.all(8.0),
                child: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.start,
                  alignment: WrapAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // Image(image: NetworkImage("https://janral.com/wp-content/uploads/2023/04/Physicswallah-logo.webp"))

                        Image.asset('assets/images/cpclogo.png', scale: 4,)
                      ],
                    ),
                    // ),
                    Lottie.network(
                        "https://assets6.lottiefiles.com/private_files/lf30_TBKozE.json",
                        height: 250, width: 250),
                  ],
                ),
              ),

            Padding(
              padding: const EdgeInsets.only(top: 50),
              child: Center(child: Text("Welcome", style: TextStyle(
                  color: Colors.deepPurple,
                  fontWeight: FontWeight.bold,
                  fontSize: 25),)),
            ),
            SizedBox(
              height: 5,
            ),

            Center(child: Text("to Concept Point Classes", style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 25,
                letterSpacing: 0.1),)),
            SizedBox(
              height: 10,
            ),


            Center(child: Text(
              "Best Teachers | Affordable Pricing | Live Batches | DPP | Notes",
              style: TextStyle(fontSize: 10,),)),
            SizedBox(
              height: 25,
            ),


            Padding(
              padding: const EdgeInsets.all(20),
              child: TextField(
                controller: phoneController,
                keyboardType: TextInputType.number,
                maxLength: 10,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)
                    ),
                    prefixIcon: Icon(Icons.call),
                    labelText: "Enter your phone number"
                ),
              ),
            ),



            Padding(
              padding: const EdgeInsets.all(30.0),
              child: SizedBox(
                height: 50,
                child: ElevatedButton(
                    onPressed: () async {
                      try {
                        await FirebaseAuth.instance.verifyPhoneNumber(
                          phoneNumber: '+91${phoneController.text}',
                          verificationCompleted: (PhoneAuthCredential credential) {},
                          verificationFailed: (FirebaseAuthException e) {},
                          codeSent: (String verificationId, int? resendToken) {
                            PhoneNumber.verify=verificationId;
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>  OtpPage(phoneNumber:phoneController.text),));
                            storeNumber();
                            Fluttertoast.showToast(msg: "Sent OTP");
                          },
                          codeAutoRetrievalTimeout: (String verificationId) {},
                        );
                      }

                      catch(e){
                        Fluttertoast.showToast(msg: "OTP Failed");
                      }
                    },style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.deepPurple, side: BorderSide.none, shape: StadiumBorder()),
                    child: const Text("Send Otp")
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
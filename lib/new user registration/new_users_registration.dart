import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:conceptpointclasses/bottom%20navigation/bottom_navigation.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../screen pages/users_goal.dart';

class NewUsersRegistration extends StatefulWidget {
  const NewUsersRegistration({super.key});

  @override
  State<NewUsersRegistration> createState() => _NewUsersRegistrationState();
}

class _NewUsersRegistrationState extends State<NewUsersRegistration> {
  //store in database user of gender variable
  String? gender;

  //get here step code 2
  //for store in database user of id and phone number
  String? uid = "";
  int? number;

  //get here step code 2
  @override
  void initState() {
    super.initState();
    getData();
  }

  //get here step code 2
  Future getData() async {
    var shareprfe = await SharedPreferences.getInstance();
    setState(() {
      number = shareprfe.getInt('number');
      uid = shareprfe.getString('uid');
    });
  }

  //Controller of field
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();

//check user if not fill data before click on submit button
  validate() {
    if (nameController.text.length <= 3) {
      Fluttertoast.showToast(msg: 'invalid name');
    } else if (!emailController.text.contains('@gmail.com')) {
      Fluttertoast.showToast(msg: 'invalid email');
    } else {
      insertIntoFirebase();
    }
  }

  //store data firebase code
  insertIntoFirebase() {
    try {
      var uId = FirebaseAuth.instance.currentUser?.uid;
      FirebaseFirestore.instance.collection("cpcusers").doc(uId).set({
        "userId": uid,
        "name": nameController.text,
        "email": emailController.text,
        "number": number,
        "gender": gender,
      });
      Fluttertoast.showToast(msg: "Register successful");
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => UsersGoal(),
          ));
    } catch (e) {
      Fluttertoast.showToast(msg: 'Something went wrong');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
      children: [
        Container(
          child: Stack(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 200,
                  width: 430,
                  color: Colors.deepPurple,
                  child: Center(
                      child: Text(
                    'Register Now',
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  )),
                ),
              ),
              Positioned(
                child: Padding(
                  padding: const EdgeInsets.all(30),
                  child: Container(
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(10)),
                    child: Card(
                      elevation: 15,
                      margin: EdgeInsets.only(top: 200),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(20),
                            child: TextField(
                              controller: nameController,
                              keyboardType: TextInputType.name,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  prefixIcon: Icon(Icons.person),
                                  labelText: "Enter your name"),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(20),
                            child: TextField(
                              controller: emailController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  prefixIcon: Icon(Icons.email),
                                  labelText: "Enter your email"),
                            ),
                          ),
                          Wrap(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 30, bottom: 10),
                                child: Text(
                                  'Select Gender',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Divider(),
                              RadioListTile(
                                title: Text("Male"),
                                value: "male",
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value.toString();
                                  });
                                },
                              ),
                              RadioListTile(
                                title: Text("Female"),
                                value: "female",
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = value.toString();
                                  });
                                },
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(30.0),
                            child: SizedBox(
                              height: 50,
                              width: 200,
                              child: ElevatedButton(
                                  onPressed: () {
                                    validate();
                                  },
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.deepPurple,
                                      side: BorderSide.none,
                                      shape: StadiumBorder()),
                                  child: const Text("Submit")),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    ));
  }
}

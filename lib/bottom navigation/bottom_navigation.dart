import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:conceptpointclasses/bottom%20navigation/study.dart';
import 'package:conceptpointclasses/bottom%20navigation/test.dart';
import 'package:conceptpointclasses/drawer%20screen/drawer_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Batches.dart';
import 'Centres.dart';
import 'books.dart';

class BottomNavigationPw extends StatefulWidget {
  const BottomNavigationPw({super.key});

  @override
  State<BottomNavigationPw> createState() => _BottomNavigationPwState();
}

class _BottomNavigationPwState extends State<BottomNavigationPw> {

//bottomNavigation
  int selectedPageIndex = 2;

var screens = [
const StudyPage(),
const CentresPage(),
const BatchesPage(),
const TestPage(),
const BooksPage(),
];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
      backgroundColor: Colors.deepPurple,

      ),
      body: screens[selectedPageIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: selectedPageIndex,
        selectedItemColor: Colors.deepPurple,

        items: const [
          BottomNavigationBarItem(
              backgroundColor: Colors.white,
              icon: Icon(Icons.school,color: Colors.deepPurple,),
              label: 'Study',),
          BottomNavigationBarItem(
              backgroundColor: Colors.white,
              icon: Icon(Icons.home,color: Colors.deepPurple,),
              label: 'Centres'),
          BottomNavigationBarItem(
              backgroundColor: Colors.white,
              icon: Icon(Icons.group,color: Colors.deepPurple,),
              label: 'Batches'),
          BottomNavigationBarItem(
              backgroundColor: Colors.white,
              icon: Icon(Icons.quiz,color: Colors.deepPurple,),
              label: 'Test'),
          BottomNavigationBarItem(
              backgroundColor: Colors.white,
              icon: Icon(Icons.book,color: Colors.deepPurple,),
              label: 'Books')
        ],
        onTap: (index) {
          selectedPageIndex = index;
          setState(() {});
        },
      ),
    );
  }
}
